Comment déterminer la taille de la valve du robinet de cuisine

Conception de cuisine

Avant de pouvoir remplacer votre vanne de robinet domestique, vous devez identifier la taille des conduites d'alimentation. La plupart des robinets utilisent des entrées filetées 1/2" MPT ou peut-être 1/2" FPT. Ces deux types de contacts sont conformes à la norme NPT (National Pipe Thread). Cependant, plusieurs robinets peuvent avoir une taille ou un filetage différent.

  Achetez la bonne vanne

La taille des conduites d'alimentation dépend du type de robinet et de conduite d'eau. Par exemple, un évier mural nécessite une vanne d'arrêt à angle droit, tandis qu'un évier au sol nécessite une vanne d'arrêt droite. Vous devez également réfléchir au type de conduite d'eau dont vous disposez, car vous devez vous procurer la bonne valve. Si vous utilisez un tuyau d'eau rigide de 1/2 pouce, vous devrez avoir un raccord à compression. D'autre part, une conduite d'eau filetée en fer galvanisé aura besoin de fils féminins.

Conception de cuisine

La taille de vanne dont vous avez besoin est généralement répertoriée dans le manuel du robinet, de sorte que vous pouvez rapidement déterminer la meilleure. [robinetterie cuivre](https://www.nivito.fr/) Cependant, une personne doit être consciente qu'une valve mâle a un diamètre plus grand que le dispositif femelle habituel, et qu'un régulateur femelle a également une dimension plus petite. Si vous avez besoin de plus d'informations sur les tailles de vannes, moen et homedepot proposent des guides utiles.

  Connectez-vous avec la collecte de l'approvisionnement en eau

Une vanne ANCHOR a généralement une taille de 3/4 de pouce. L'orifice de décharge mesure 3/8 de pouce de diamètre. Cette taille est courante pour les robinets d'évier et la conduite d'alimentation en eau principale. Selon le type de robinet, il peut avoir d'autres tailles pour la conduite d'alimentation. Par exemple, un robinet Kohler a une conduite d'alimentation composée de conduites d'alimentation de 1/2 pouce, tandis qu'un robinet Delta utilise des raccords à compression de 3/8 pouce pour pouvoir se connecter à la conduite d'alimentation en eau potable.

Design d'intérieur de cuisine

Les nouveaux robinets devraient avoir des tubes d'alimentation, mais ils pourraient ne pas être assez longs. Les tubes d'alimentation peuvent en outre ne pas avoir les filetages les plus appropriés pour se connecter avec la vanne d'arrêt. Il existe également plusieurs types de tuyaux d'alimentation. Les plus fréquents sont en plastique polyvalent ou en métal tressé. Celles en métal durent plus longtemps et sont recommandées lors de l'utilisation d'eau chaude.

  Mesurez généralement la longueur

La longueur particulière de la ligne d'alimentation particulière doit être mesurée dans l'orifice de décharge de la vanne d'arrêt d'angle jusqu'au dessous de l'orifice d'admission avec le robinet. Cette mesure spécifique devrait permettre des lignes supplémentaires. Selon la façon dont vous prévoyez de tracer la ligne d'approvisionnement, une personne peut avoir besoin d'acheter des tuyaux supplémentaires. Vous pouvez utiliser un cutter en PVC de type cliquet pour mesurer la longueur. Une fois que vous avez déterminé la longueur de la ligne d'alimentation, glissez l'écrou de fixation, le support de compression et la bague en diamant en plastique sur la petite extrémité de la ligne d'alimentation. Assurez-vous que l'extrémité biseautée de la conduite d'alimentation est généralement alignée contre la base de l'orifice d'admission. Serrez ensuite l'écrou de blocage pour éviter tout type de fuite.

Design d'intérieur de cuisine

Les vannes plus anciennes sont difficiles à remplacer. Lorsque la reconstruction d'un dispositif de contrôle peut résoudre des problèmes pendant des années, vous pouvez plutôt envisager d'installer un nouveau robinet à tournant sphérique quart de tour plus moderne. Ceux-ci coulent rarement, et ils sont faciles à installer. Et la bonne nouvelle est qu'ils ne nécessitent qu'une heure de vos règles.
